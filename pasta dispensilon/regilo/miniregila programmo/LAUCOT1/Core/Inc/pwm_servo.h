/*
 * pwm_servo.h
 *
 *  Created on: Oct 26, 2020
 *      Author: serp
 */

#ifndef INC_PWM_SERVO_H_
#define INC_PWM_SERVO_H_

#include <stdint.h>
#include <stdbool.h>

void Servo_init();
void ServoStart(double angle1, double angle2);
void Servo1GoTo(double angle);
void Servo2GoTo(double angle);
void Servo1MoveTo(double angle, uint32_t time_ms);
void Servo2MoveTo(double angle, uint32_t time_ms);
bool Servo1_is_reached();
bool Servo2_is_reached();

void ServoIRQ(void);

#endif /* INC_PWM_SERVO_H_ */
